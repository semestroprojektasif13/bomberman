package view;

import com.google.common.util.concurrent.SettableFuture;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import org.loadui.testfx.utils.FXTestUtils;
import org.loadui.testfx.utils.UserInputDetector;
import view.group.*;

import java.util.concurrent.TimeUnit;

import static org.junit.Assume.assumeTrue;

/**
 * Created by Aivaras on 5/19/2016.
 */
public class TestMainWindow extends GuiTest {
    private static final SettableFuture<Stage> stageFuture = SettableFuture.create();
    public static class TestMW extends MainWindow{
        @Override
        public void start(Stage primaryStage){
            try {
                super.start(primaryStage);
                stageFuture.set(primaryStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Before
    public void setupStage(){
        assumeTrue(!UserInputDetector.instance.hasDetectedUserInput());
        FXTestUtils.launchApp(TestMW.class);
        try {
            stage = targetWindow(stageFuture.get(25, TimeUnit.SECONDS));
            FXTestUtils.bringToFront(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Parent getRootNode() {
        return stage.getScene().getRoot();
    }
    @Test
    public void mainGroupNavigation(){
        //Test Single Player button click
        assert(((Button)find("#mainG_Single")).getText().equals("Single Player"));
        click("#mainG_Single");
        assert(getRootNode().getClass().equals(MapGroup.class));
        click("#mapG_Choose");
        assert(getRootNode().getClass().equals(ChooseMapGroup.class));
        Button button = find("#chooseMG_Edit");
        assert (button.isDisable());
        click("#chooseMG_Cancel");
        assert(getRootNode().getClass().equals(MapGroup.class));
        click("#mapG_Back");
        assert(getRootNode().getClass().equals(MainGroup.class));
        //Test multiplayer button click
        assert(((Button)find("#mainG_Multiplayer")).getText().equals("Multiplayer"));
        click("#mainG_Multiplayer");
        assert(getRootNode().getClass().equals(MultiplayerSettingsGroup.class));
        click("#multiG_Cancel");
        assert(getRootNode().getClass().equals(MainGroup.class));
        //Test About button click
        assert (((Button)find("#mainG_About")).getText().equals("About"));
        click("#mainG_About");
        assert (getRootNode().getClass().equals(AboutGroup.class));
        click("#aboutG_Back");
        assert(getRootNode().getClass().equals(MainGroup.class));
    }

}
