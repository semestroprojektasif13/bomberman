package view.background;

import com.sun.deploy.util.FXLoader;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Button;
import org.junit.Before;
import org.junit.Test;
import view.components.CustomButtons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Aivaras on 5/22/2016.
 */
public class TestButtons {
    @Before
    public void before(){
        new JFXPanel();
    }
    @Test
    public void testExitButtonHeight() {
        DoubleProperty w = new SimpleDoubleProperty(800);
        DoubleProperty h = new SimpleDoubleProperty(800);
        assertNotNull(CustomButtons.exitButton(w, h));

    }
    @Test
    public void testCustomButtonText() {
        Button btn = CustomButtons.basicButton("sss",200);
        assertEquals("sss", btn.getText());
    }
    @Test
    public void testCustomButtonWidth() {
        Button btn = CustomButtons.basicButton("sss",200);
        assertEquals(200, (int)btn.getPrefWidth());
    }
}
