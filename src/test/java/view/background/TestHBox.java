package view.background;

import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Button;
import org.junit.Before;
import org.junit.Test;
import view.components.CustomHBox;

import static junit.framework.TestCase.assertNotNull;

/**
 * Created by Aivaras on 5/22/2016.
 */
public class TestHBox {
    @Before
    public void before(){
        new JFXPanel();
    }
    @Test(expected = NullPointerException.class)
    public void testHBoxException(){
        Button btn[] =new Button[1];
        assertNotNull(CustomHBox.setupHBox(btn));
    }
    @Test
    public void testHBox(){
        Button btn[] =new Button[1];
        btn[0] = new Button("w");
        assertNotNull(CustomHBox.setupHBox(btn));
    }
}
