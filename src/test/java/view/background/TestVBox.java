package view.background;

import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import org.junit.Before;
import org.junit.Test;
import view.components.CustomVBox;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Aivaras on 5/22/2016.
 */
public class TestVBox {
    @Before
    public void before(){
        new JFXPanel();
    }
    @Test
    public void testClass(){
        Button btn[] = new Button[1];
        btn[0] = new Button();
        assertEquals(VBox.class, CustomVBox.setupVBox(btn,200).getClass());
    }
}
