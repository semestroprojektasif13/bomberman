package view.background;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.embed.swing.JFXPanel;
import org.junit.Before;
import org.junit.Test;
import view.components.CustomBackgrounds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Aivaro on 09/04/2016.
 */
public class TestBackground {
    @Before
    public void before(){
        new JFXPanel();
    }
    @Test
    public void testRectangleBg(){
        DoubleProperty w = new SimpleDoubleProperty(800);
        DoubleProperty h = new SimpleDoubleProperty(800);
        assertNotNull(CustomBackgrounds.ovalRectangleBg(w,h));
    }

    @Test
    public void testRectangleWidth(){
        DoubleProperty w = new SimpleDoubleProperty(800);
        DoubleProperty h = new SimpleDoubleProperty(800);
        assertEquals(800,(int)CustomBackgrounds.ovalRectangleBg(w,h).getWidth());
    }

    @Test
    public void testRectangleHeight(){
        DoubleProperty w = new SimpleDoubleProperty(800);
        DoubleProperty h = new SimpleDoubleProperty(800);
        assertEquals(800,(int)CustomBackgrounds.ovalRectangleBg(w,h).getHeight());
    }
}

