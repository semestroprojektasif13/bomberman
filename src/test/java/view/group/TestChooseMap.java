package view.group;

import database.utility.DatabaseTable;
import database.utility.MapData;
import database.utility.QueryExecutor;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test cases for {@link ChooseMapGroup}.
 *
 * @author Aivaras Dziaugys
 */
public class TestChooseMap {

    private ChooseMapGroup chooseMapGroup;
    private MapData mapData1;
    private MapData mapData2;

    @Before
    public void before() {
        new JFXPanel();

        DoubleProperty w = new SimpleDoubleProperty(800);
        DoubleProperty h = new SimpleDoubleProperty(800);
        chooseMapGroup = new ChooseMapGroup(w,h,ChooseMapGroup.SINGLE_PLAYER);
        mapData1 = new MapData(QueryExecutor
                .getUniqueIdentification(DatabaseTable.MAPS_TABLE), 11, 11, "data1", "name1");
        mapData2 = new MapData(QueryExecutor
                .getUniqueIdentification(DatabaseTable.MAPS_TABLE) + 1, 12, 12, "data2", "name2");
    }

    @Test
    public void testListView() {
        assertNotNull(chooseMapGroup.mapView());
    }

    @Test
    public void testAddListWithOne() {

        assertEquals(mapData1, chooseMapGroup.addMap(mapData1));
    }

    @Test
    public void testAddListWithTwo() {
        int beforeCount = chooseMapGroup.mapView().getItems().size();
        assertEquals(mapData1, chooseMapGroup.addMap(mapData1));
        assertEquals(mapData2, chooseMapGroup.addMap(mapData2));
        beforeCount += 2;
        chooseMapGroup.mapView().getItems().forEach(System.out::print);
        assertEquals(beforeCount, chooseMapGroup.mapView().getItems().size());
    }


    @Test
    public void testVBox() {
        assertNotNull(ChooseMapGroup.setupVBox(new HBox(), new ListView<>()));
    }
}
