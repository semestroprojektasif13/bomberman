package opencvTest.coreTest;

import opencvModule.core.ShapeRecognizer;
import opencvModule.core.VideoCaptureUtilities;
import org.junit.Test;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Saulius Stankevičius.
 * Class for testing ShapeRecognizer methods
 */
public class VideoCaptureUtilitiesTest
{

    private VideoCapture cap;

    @Test
    public void testOpenCapture1()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        cap = VideoCaptureUtilities.startCapture();
        if(cap != null)
        {
            assertEquals(cap.isOpened(), true);
            cap.release();
        }
        else
        {
            assertEquals(false, true);
        }
    }

    @Test
    public void testCloseCapture1()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        cap = VideoCaptureUtilities.startCapture();

        assertEquals(0, VideoCaptureUtilities.stopCapture(cap));
        cap.release();
    }
}
