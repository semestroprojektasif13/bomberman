package opencvTest.dataStructureTest;

import opencvModule.dataStructure.Matrix;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Saulius Stankevičius.
 * Class for testing Matrix methods
 */
public class MatrixTest
{
    @Test
    public void testIfClassExists()
    {
        Matrix matrix = new Matrix<>(0, 0);

        assertNotNull(matrix.testMethod());
    }

    @Test(expected = RuntimeException.class)
    public void testAdd1()
    {
        Matrix<Integer> matrix = new Matrix<>(3, 3);

        matrix.add(9, 9, 9);
    }

    @Test(expected = RuntimeException.class)
    public void testAdd2()
    {
        Matrix<Integer> matrix = new Matrix<>(3, 3);

        for(int i = 0; i < 99; i++)
        {
            matrix.add(i);
        }
    }

    @Test
    public void testAdd3()
    {
        Matrix<Integer> matrix = new Matrix<>(3, 3);

        matrix.add(1);
        matrix.add(2);
        matrix.add(3);

        assertEquals(new Integer(2), matrix.get(1, 0));
    }

    @Test
    public void testToString1()
    {
        Matrix<Integer> matrix = new Matrix<>(3, 2);

        matrix.add(1);
        matrix.add(2);

        assertEquals("3 2 120000", matrix.toString());
    }
}
