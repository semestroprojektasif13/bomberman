package view;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import view.group.ChooseImageGroup;
import view.group.MainGroup;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main window class
 * launches the application
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class MainWindow extends Application {

    private static Stage stage;
    private static final double SCREEN_WIDTH = 800;
    private static final double SCREEN_HEIGHT = 600;
    private static DoubleProperty screenWidth = new SimpleDoubleProperty(SCREEN_WIDTH);
    private static DoubleProperty screenHeight = new SimpleDoubleProperty(SCREEN_HEIGHT);
    private static final Logger log = Logger.getLogger(ChooseImageGroup.class.getName());

    public static void main(String args[]) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        stage = primaryStage;
        stage.initStyle(StageStyle.TRANSPARENT);
        loadSettings();
        Group root = new MainGroup(screenWidth, screenHeight);
        Scene scene = new Scene(root, screenWidth.get(), screenHeight.get(), Color.rgb(0,0,0,0));
        File f = new File("customStyleSheet.css");
        scene.getStylesheets().clear();
        scene.getStylesheets().add("file:///" + f.getAbsolutePath().replace("\\", "/"));

        final Delta dragDelta = new Delta();
        root.setOnMousePressed(mouseEvent -> {
            dragDelta.x = stage.getX() - mouseEvent.getScreenX();
            dragDelta.y = stage.getY() - mouseEvent.getScreenY();
        });
        root.setOnMouseDragged(mouseEvent -> {
            stage.setX(mouseEvent.getScreenX() + dragDelta.x);
            stage.setY(mouseEvent.getScreenY() + dragDelta.y);
        });
        stage.setScene(scene);
        stage.show();
    }

    /**
     * setup scene to display
     *
     * @param root - layout to add to scene
     */
    public static void chooseScene(Group root) {
        Scene scene = new Scene(root, screenWidth.get(), screenHeight.get(), Color.rgb(0,0,0,0));
        final Delta dragDelta = new Delta();
        root.setOnMousePressed(mouseEvent -> {
            dragDelta.x = stage.getX() - mouseEvent.getScreenX();
            dragDelta.y = stage.getY() - mouseEvent.getScreenY();
        });
        root.setOnMouseDragged(mouseEvent -> {
            stage.setX(mouseEvent.getScreenX() + dragDelta.x);
            stage.setY(mouseEvent.getScreenY() + dragDelta.y);
        });

        File f = new File("customStyleSheet.css");
        scene.getStylesheets().clear();
        scene.getStylesheets().add("file:///" + f.getAbsolutePath().replace("\\", "/"));
        stage.setScene(scene);
        stage.show();
    }

    /**
     * load settings setup by user from settings file
     */
    private void loadSettings() {
        File file = new File("custom.settings");
        if(!file.exists()){
            return;
        }
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (int i = 0; i < 5; i++) {
                String size = br.readLine();
                if(i==4){
                    screenWidth.set(Double.valueOf(size.split(" ")[0]));
                    screenHeight.set(Double.valueOf(size.split(" ")[1]));
                }
            }
        } catch (FileNotFoundException e) {
            log.log(Level.FINE, "no settings file found");
        } catch (IOException e) {
            log.log(Level.FINE, "can not write to settings file");
        }
    }

    /**
     * inner class to save drag coordinates
     */
    static class Delta{
        double x, y;
    }
}
