package view.components;

import javafx.beans.property.DoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.io.*;

/**
 * Class for customized background design
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class CustomBackgrounds {
    /**
     * set up custom background
     * @param width window width
     * @param height window height
     * @return rectangle background
     */
    public static Rectangle ovalRectangleBg(DoubleProperty width, DoubleProperty height) {
        Rectangle rectangle = new Rectangle(width.get(), height.get());
        rectangle.setX(0);
        rectangle.setY(0);
        rectangle.getStyleClass().add("rectangle-background");
        String s[] = new String[4];

        File file = new File("custom.settings");
        if(file.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                for (int i = 0; i < 4; i++) {
                    s[i] = br.readLine();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            rectangle.setFill(Color.rgb(Integer.valueOf(s[0]), Integer.valueOf(s[1]), Integer.valueOf(s[2]), Double.valueOf(s[3])));
        }
        return rectangle;
    }
}
