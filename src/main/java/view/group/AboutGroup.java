package view.group;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import view.MainWindow;
import view.components.CustomBackgrounds;
import view.components.CustomButtons;

import java.io.*;

/**
 * Creates About window layout as a Group object
 * load text about the game to text area
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class AboutGroup extends Group {
    private TextArea textArea = new TextArea();
    private Button backButton;
    private DoubleProperty width = new SimpleDoubleProperty(800);
    private DoubleProperty height = new SimpleDoubleProperty(600);


    /**
     * Group constructor
     *
     * @param width  - window width
     * @param height - window height
     */
    public AboutGroup(DoubleProperty width, DoubleProperty height) {
        this.width = width;
        this.height = height;
        setButton();
        setTextArea();
        this.getChildren().addAll(CustomBackgrounds.ovalRectangleBg(width, height), textArea, backButton, CustomButtons.exitButton(width, height),CustomButtons.exitButton(width,height));
    }

    /**
     * setup back button - returns user to previous screen
     */
    private void setButton() {
        backButton = CustomButtons.basicButton("Back", width.get() * .2);
        backButton.setId("aboutG_Back");
        backButton.setLayoutY(height.get() * .8);
        backButton.setLayoutX(width.get() * .1);
        backButton.setOnAction(event -> MainWindow.chooseScene(new MainGroup(width,height)));
    }

    /**
     * setup text area and load text from file with information about the game
     */
    private void setTextArea() {


        textArea = new TextArea();
        textArea.setPrefWidth(width.get() * .8);
        textArea.setPrefHeight(height.get() * .6);
        textArea.setLayoutY(height.get() * .1);
        textArea.setLayoutX(width.get() * .1);
        textArea.setWrapText(true);
        textArea.setFont(Font.font(16));

        try (BufferedReader br = new BufferedReader(new FileReader("Bomberman documents/about.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
            textArea.setText(everything);
            textArea.setEditable(false);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
