package view.group;

import bomberman.Game;
import database.utility.MapData;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import level_editor.Editor;
import opencvModule.Core;
import opencvModule.imageOut.ImagePanelInterface;
import view.MainWindow;
import view.components.CustomBackgrounds;
import view.components.CustomButtons;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Creates window layout as a Group object
 * GUI allows to choose image from computer or take a photo to setup map
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class ChooseImageGroup extends Group implements ImagePanelInterface {

    private static final Logger log = Logger.getLogger(ChooseImageGroup.class.getName());
    private static ImageView imageView = new ImageView();
    private StringProperty filepath = new SimpleStringProperty();
    private File file;
    private StringProperty mapString = new SimpleStringProperty();
    private static Core core;
    private final String[] buttonNames = {"Browse","Take a photo","Edit","Load","Cancel" };
    private DoubleProperty width = new SimpleDoubleProperty();
    private DoubleProperty height= new SimpleDoubleProperty();
    private MapData mapData;


    /**
     * Constructor - set up initial window for image choosing
     */

    public ChooseImageGroup(DoubleProperty width, DoubleProperty height) {
        this.width = width;
        this.height = height;
        ArrayList<String> imageBuffer = new ArrayList<>();
        imageBuffer.add("images\\blackCell.png");
        imageBuffer.add("images\\brickWall.png");
        imageBuffer.add("images\\steelWall.png");
        core = new Core(this, imageBuffer);

        loadImage();
        file = new File(filepath.get());
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(width.get()*.6);
        imageView.setFitHeight(height.get()*.6);
        VBox imageBox = new VBox();
        imageBox.setMinWidth(width.get()*.6);
        imageBox.getChildren().add(imageView);

        HBox window = new HBox(50);
        window.setPadding(new Insets(20));
        window.getChildren().addAll(imageBox, setupSidePanel());
        this.getChildren().addAll(CustomBackgrounds.ovalRectangleBg(width, height), window, CustomButtons.exitButton(width, height));
    }

    /**
     * setup vertical box side panel with control buttons
     *
     * @return - right side panel
     */
    private VBox setupSidePanel() {
        VBox sideBox = new VBox(20);
        sideBox.setAlignment(Pos.TOP_RIGHT);
        Button[] btn = setupButtons(buttonNames,200);

        sideBox.getChildren().addAll(btn);
        sideBox.setMinWidth(200);
        return sideBox;
    }
    public Button[] setupButtons(String[] buttonNames, double w) {
        Button[] btnGroup = new Button[buttonNames.length];
        int i = 0;
        for (String s : buttonNames) {
            btnGroup[i] = CustomButtons.basicButton(s, w);
            btnGroup[i].setId("chooseImgG_"+s.split(" ")[0]);
            btnGroup[i].setOnAction(this::eventHandler);
            i++;
        }
        return btnGroup;
    }

    /**
     * set global variable filepath with initial image path if empty
     * setup image view to show image with filepath specified
     */
    private void loadImage() {
        String url = null;
        try {
            if (!filepath.isNotEmpty().get()) {
                url = new File("images/Bomberman.png").toURI().toURL().toString();
                filepath.set(url);
            } else {
                url = filepath.get();
            }
        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "Image URL is malformed");
        }
        Image image = new Image(url, width.get()*.6, height.get()*.6, true, true, true);
        imageView.setImage(image);
    }

    @Override
    public void refreshImage(java.awt.Image image) {
        WritableImage tmp = new WritableImage((int)(width.get()*.6), (int)(height.get()*.6));
        java.awt.image.BufferedImage img = (BufferedImage) image;
        Image ii = javafx.embed.swing.SwingFXUtils.toFXImage(img, tmp);
        imageView.setImage(ii);

    }

    /**
     * button event handler
     * @param e button click
     */
    private void eventHandler(ActionEvent e) {
        switch (((Button) e.getSource()).getText()) {
            case ("Take a photo"):
                core.analyze();
                ((Button) e.getSource()).setText("Capture");
                break;
            case ("Browse"):
                chooseFile();
                break;
            case ("Capture"):
                mapData = core.getMapData();
                core.stop();
                if (mapData != null) {
                    mapString.set(mapData.getMapData());
                    ((Button) e.getSource()).setText("Take a photo");
                } else {
                    ((Button) e.getSource()).setText("Take a photo");
                }
                break;
            case("Edit"):
                if(mapData!=null) {
                    new Editor(mapData);
                    Platform.exit();
                }
                break;
            case("Load"):
                if(mapData!=null) {
                    new Game(mapData);
                }
                break;
            case ("Cancel"):
                MainWindow.chooseScene(new MapGroup(width,height));
                break;
            default:
                log.log(Level.WARNING, "Something is wrong with image capture");
                break;
        }
    }

    /**
     * open file chooser window to load image to convert to map string
     */
    private void chooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose map image to scan");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir") + File.separator));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        file = fileChooser.showOpenDialog(null);
        try {
            if (file != null) {
                filepath.set(file.toURI().toURL().toString());
            }
            //29 is the place there the actual path to the image starts
            core.analyze(filepath.toString().substring(29, filepath.toString().length() - 1));
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e1) {
                log.log(Level.WARNING, "TimeUnit exception at choosing image");
            }
            mapData = core.getMapData();
            mapString.set(mapData.toString());
            core.stop();
        } catch (MalformedURLException me) {
            log.log(Level.WARNING, "URL gotten from file chooser is malformed");
        }
    }
}
