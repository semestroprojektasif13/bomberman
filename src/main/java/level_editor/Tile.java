package level_editor;

/**
 * Created by bruar on 5/16/2016.
 */
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

public class Tile implements Cloneable {
    private int r = 0;
    private int plr = 0;
    private int posX;
    private int posY;
    private boolean selected = false;
    private String name;
    private String type = "floor";
    private BufferedImage image;

    public Tile(File file, int tilecounter) {
        this.setPosX(((tilecounter % 1) * 45) + (tilecounter % 1 + 1) * 2);
        this.setPosY(((tilecounter / 1) * 45) + (tilecounter / 1 + 1) * 4);
        this.setName(file.getName());

        if (file.getName().endsWith("brickWall.png")) this.setR('1');
        else if (file.getName().endsWith("steelWall.png")) this.setR('2');
        else if (file.getName().endsWith("enemy.png")) this.setR('3');
        else if (file.getName().endsWith("floorempty.png")) this.setR('0');
        else if (file.getName().endsWith("players.png")) this.setR('4');
        selected = true;

        image = EditorGraphics.loadImage(file.getAbsolutePath());
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (Exception e) {
            return null;
        }
    }

    public void draw(Graphics2D g2d, int offX, int offY) {
        int x = posX + offX;
        int y = posY + offY;

        g2d.drawImage(image, x, y, 45, 45, null);

        if (selected) {
            g2d.setColor(Color.red);
            g2d.drawRect(x - 1, y - 1, 46, 46);
        }
    }

    public void drawOnMap(Graphics2D g2d, int offX, int offY) {
        int x = posX + offX;
        int y = posY + offY;
        g2d.drawImage(image, x, y, 29, 29, null);
    }

    public int getPosX() {
        return posX;
    }

    public void setR(int rr) {
        this.r = rr;
    }

    public int getR() {
        return r;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int y) {
        this.posY = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public void deselect() {
        selected = false;
    }

    public BufferedImage getImage() {
        return this.image;
    }

    public void select() {
        selected = true;
    }

    public int getPlr() {return plr;}
    public void setPlr(int i) {this.plr = i;}

}

