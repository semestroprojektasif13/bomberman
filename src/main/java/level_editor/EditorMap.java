package level_editor;

/**
 * Created by bruar on 5/16/2016.
 */

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.File;
import java.util.Vector;

public class EditorMap {
    private int width;
    private int height;
    private int tilesX;
    private int tilesY;
    private String name;
    private Vector<Tile> tileListL1 = new Vector<Tile>();

    public EditorMap(int w, int h, String nm) {
        tilesX = w;
        tilesY = h;
        width = w * 30;
        height = h * 30;
        name = nm;
        int tiles = 0;
        File defaultF = new File("images/floorempty.png");
        for (int i = 0; i < w * h; i++) {
            Tile tile = new Tile(defaultF, tiles);
            tile.setPosX(30 * (i % tilesX));
            tile.setPosY(30 * (i / tilesX));
            tileListL1.addElement(tile);
            tiles++;
        }

    }
    public EditorMap(int w, int h, Vector<Integer> tls,String nm) {
        tilesX = w;
        tilesY = h;
        width = w * 30;
        height = h * 30;
        name = nm;
        int tiles = 0;
        File defaultF = new File("images/floorempty.png");
        File soft = new File("images/brickWall.png");
        File hard = new File("images/steelWall.png");
        File enemy = new File("images/enemy.png");
        File player = new File("images/players.png");
        for(int i = 0; i < tls.size(); i++){
            Tile tile;
            switch (tls.get(i)){
                case 0: tile = new Tile(defaultF, tiles); break;
                case 1: tile = new Tile(soft, tiles); break;
                case 2: tile = new Tile(hard, tiles); break;
                case 3: tile = new Tile(enemy, tiles); break;
                case 4: tile = new Tile(player, tiles); break;
                default: tile = new Tile(defaultF, tiles);
            }
            tile.setPosX(30 * (i % tilesX));
            tile.setPosY(30 * (i / tilesX));
            tileListL1.addElement(tile);
            tiles++;
        }
    }
    public void draw(Graphics2D g2d) {
        for (int i = 0; i < tileListL1.size(); i++) {
            tileListL1.get(i).drawOnMap(g2d, 0, 0);
        }
    }
    public void addTile(int x, int y, Tile drawTile) {
        Tile newTile = (Tile) drawTile.clone();
        int pos = x  + y * tilesX;
        newTile.setPosX(x * 30);
        newTile.setPosY(y * 30);
        tileListL1.set(pos, newTile);
    }
    public void addRowTile(){
        if (tilesY >= 20) return;
        int tiles = 0;
        File defaultF = new File("images/floorempty.png");
        for (int i = 0; i < this.getWidth()/30; i++) {
            Tile tile = new Tile(defaultF, tiles);
            tile.setPosX(30 * (i % tilesX));
            tile.setPosY(30 * (i / tilesX) + this.getHeight());
            tileListL1.addElement(tile);
            tiles++;
        }
        this.setHeight(this.getHeight()+30);
        tilesY++;
    }
    public void removeRowTile(){
        if (tilesY <= 5) return;
        for (int i = 0; i < this.getWidth()/30; i++) {
            tileListL1.remove(tileListL1.size()-1);
        }
        this.setHeight(this.getHeight()-30);
        tilesY--;
    }
    public void removeColTile(){
        if (tilesX <= 5) return;
        Vector<Tile> temp = new Vector<Tile>();
        for (int i = 0; i < tilesY; i++) {
            for(int j = 0; j < tilesX-1; j++) temp.addElement(tileListL1.get(j+i*tilesX));
        }
        this.setWidth(width-30);
        tileListL1 = temp;
        tilesX--;

    }
    public void addColumnTile(){
        if (tilesX >= 20) return;
        int tiles = 0;
        Vector<Tile> temp = new Vector<>();
        File defaultF = new File("images/floorempty.png");
        for (int i = 0; i < tilesY; i++) {
            for(int j = 0; j < tilesX; j++) temp.addElement(tileListL1.get(j+i*tilesX));
            Tile tile = new Tile(defaultF, tiles);
            tile.setPosX(this.getWidth());
            tile.setPosY(30 * i);
            temp.addElement(tile);
            tiles++;
        }
        tileListL1 = temp;
        this.setWidth(this.getWidth()+30);
        tilesX++;
    }
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Vector<Tile> getTileListL1() {
        return tileListL1;
    }
    public int getTilesX(){return tilesX;}
    public int getTilesY() {return  tilesY;}
    public void setTilesX(int x) {this.tilesX = x;}
    public void setTilesY(int y) {this.tilesY = y;}
    public void setTileListL1(Vector<Tile> v){tileListL1=v;}
    public void setName(String nm){this.name = nm;}
    public String getName(){return  name;}

}
