package level_editor;

/**
 * Created by bruar on 5/17/2016.
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

public class Tab {
    private int posX;
    private int posY;
    private int width;
    private int height;
    private Shape tabGraphic;

    public Tab(int x, int y, int w, int h) {
        this.setPosX(x);
        this.setPosY(y);
        this.setWidth(w);
        this.setHeight(h);
        tabGraphic = new Rectangle2D.Float(x, y, w, h);
    }
    public void draw(Graphics2D g2d, boolean active) {
        if(active) {
            g2d.setColor(Color.GRAY);
            g2d.draw(tabGraphic);
            g2d.fill(tabGraphic);

            g2d.setColor(Color.white);
            g2d.setFont(new Font("Arial", Font.BOLD, 14));
            g2d.drawString("Tiles", posX + 5, posY + 15);
            g2d.drawString("Destructable wall", posX + 60, posY + 60);
            g2d.drawString("Indestructable wall", posX + 60, posY + 110);
            g2d.drawString("Enemies", posX + 60, posY + 160);
            g2d.drawString("Players", posX + 60, posY + 210);
            g2d.drawString("Floor block", posX + 60, posY + 260);
        }
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
    public int getPosX(){return posX;}

    public int getPosY(){return posY;}

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    public int getW(){return width;}
    public int getH(){return height;}
    public void setTabGraphic(Shape sh){this.tabGraphic = sh;}
    public Shape getTabGraphic() {return tabGraphic;}
}
