package level_editor;

/**
 * Created by bruar on 5/16/2016.
 */

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class FileNavigator {
    private JFileChooser tilefc;
    private File currentFile;

    public FileNavigator() {
        tilefc = new JFileChooser(new File("images/"));
        this.setFilter(tilefc, ".map", "Map");
    }

    public void setFilter(final JFileChooser jf, final String end,
                          final String text) {
        jf.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory()
                        || f.getName().toLowerCase().endsWith(end);
            }

            @Override
            public String getDescription() {
                return text;
            }

        });
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(File currentFile) {
        this.currentFile = currentFile;
    }
}
