package level_editor;

/**
 * Created by bruar on 5/23/2016.
 */
public class Mouse {
    public static boolean isInRegion(int x, int y, int startX, int startY,
                                     int endX, int endY) {
        boolean status = false;
        if ((x > startX && x < endX) && (y > startY && y < endY)) {
            status = true;
        }
        return status;
    }
}