package level_editor;

/**
 * Created by bruar on 5/16/2016.
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.Vector;
import javax.swing.JPanel;

public class TileNavigator extends JPanel implements MouseListener  {
    private Vector<Button> buttonList = new Vector<Button>();
    private String mode = "Tiles";
    private Editor editor;
    private Tab tile;
    private Vector<Tab> tabList = new Vector<Tab>();
    private TilePanel tilePanelTiles = new TilePanel(5, 30, 190, 250);

    public TileNavigator(Editor e) {
        this.setBackground(Color.GRAY);
        this.setSize(200, 360);
        this.setDoubleBuffered(true);
        addMouseListener(this);

        editor = e;

        File softBlock = new File("images/brickWall.png");
        File hardBlock = new File("images/steelWall.png");
        File enemy = new File("images/enemy.png");
        File floor = new File("images/floorempty.png");
        File play = new File("images/players.png");

        addTile(softBlock);
        addTile(hardBlock);
        addTile(enemy);
        addTile(play);
        addTile(floor);
        tabList.add(tile = new Tab(1, 0, 70, 20));
    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        //tile.draw(g2d,true);


        g2d.setColor(Color.GRAY);
        g2d.fill(new Rectangle2D.Float(0, 20, 200, 360));

        if (mode.equals("Tiles"))
            tilePanelTiles.draw(g2d);


        for (int i = 0; i < buttonList.size(); i++) {
            buttonList.get(i).draw(g2d);
        }

        g2d.setColor(Color.DARK_GRAY);
        g2d.drawLine(0, 0, 0, 900);
        tile.draw(g2d,true);
    }
    public void addTile(File file) {
        tilePanelTiles.deselectAll();
        tilePanelTiles.addTile(file);
        tilePanelTiles.deselectAll();
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO
        Tile cTile = tilePanelTiles.getTile(e.getX(), e.getY());
        editor.updateTileViewer(cTile, "paint");
        tilePanelTiles.deselectAll();
        cTile.select();
        this.repaint();
    }
    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    public String getMode() {
        return mode;
    }
    public void setButtonList(Vector<Button> btn){this.buttonList = btn;}
    public Vector<Button> getButtonList() {return buttonList;}
    public void setMode(String s){this.mode = s;}
    public void setEditor(Editor e){this.editor = e;}
    public Editor getEditor() {return editor;}
    public void setTile(Tab tl){this.tile = tl;}
    public Tab getTile()    {return tile;}
    public Vector<Tab> getTabList(){return  tabList;}
    public void setTabList(Vector<Tab> tb){this.tabList = tb;}

}
