package database.utility;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This <code>class</code> provides useful utility methods that can
 * be used in order to retrieve, add or remove data from the database.
 * In order to open a {@link Connection} this <code>class</code> uses
 * a method from {@link DatabaseConnectionHelper}.
 *
 * @author Sarunas Sarakojis
 * @see DatabaseConnectionHelper#getConnection()
 */
public class QueryExecutor {

    private QueryExecutor() {
        /*
         * This class cannot be instantiated
         * since it contains static methods only.
         */
    }

    /**
     * Adds single instance of {@link MapData} to the
     * {@link DatabaseTable#MAPS_TABLE}.
     *
     * @param mapData map data to add to the database
     */
    public static void addMapDataToTable(MapData mapData) {
        addMapDataToTable(Collections.singletonList(mapData));
    }

    /**
     * Adds multiple instances of {@link MapData} to the
     * {@link DatabaseTable#MAPS_TABLE}.
     *
     * @param mapData list of map data to be added to the database
     */
    public static void addMapDataToTable(List<MapData> mapData) {
        Connection connection = DatabaseConnectionHelper.getConnection();

        try (Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);

            addMapDataInsertionBatches(mapData, statement);
            statement.executeBatch();
            connection.commit();
        } catch (SQLException ignored) {

        } finally {
            try {
                connection.setAutoCommit(true);
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Deletes single instance of {@link MapData} from the
     * {@link DatabaseTable#MAPS_TABLE}.
     *
     * @param mapData an instance of map data to be removed
     */
    public static void deleteMapDataFromTable(MapData mapData) {
        deleteMapDataFromTable(Collections.singletonList(mapData));
    }

    /**
     * Deletes multiple instances of {@link MapData} from the
     * {@link DatabaseTable#MAPS_TABLE}.
     *
     * @param mapData list of map data to be removed from the database
     */
    public static void deleteMapDataFromTable(List<MapData> mapData) {
        Connection connection = DatabaseConnectionHelper.getConnection();

        try (Statement statement = connection.createStatement()) {
            statement.execute(getDeleteMapDataQuery(mapData));
        } catch (SQLException ignored) {

        } finally {
            DatabaseConnectionHelper.closeConnection(connection);
        }
    }

    /**
     * Method returns the count of rows int the specified <code>table</code>.
     *
     * @param table table whose rows count shall be returned
     * @return row count of the <code>table</code>
     */
    public static int getTableRowsCount(DatabaseTable table) {
        Connection connection = DatabaseConnectionHelper.getConnection();
        int rows = 0;

        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery("SELECT COUNT(*) FROM " + table);

            if (result.next()) {
                rows = result.getInt(1);
            }
        } catch (SQLException ignored) {

        } finally {
            DatabaseConnectionHelper.closeConnection(connection);
        }

        return rows;
    }

    /**
     * Method returns an unique identification from the specified <code>table</code>.
     * Actually, this is: <code> lastRowIdentification + 1 </code>. Since <em>primary
     * key</em> in the database is set to <code>AUTO_INCREMENT</code> this guarantees
     * that the returned identification is unique.
     * <strong>NOTE:</strong> if someone inserts a new row right after this method
     * call, returned identification is no longer unique.
     *
     * @param table tables from which the unique <em>identification</em> will be returned
     * @return unique identification
     */
    public static int getUniqueIdentification(DatabaseTable table) {
        Connection connection = DatabaseConnectionHelper.getConnection();
        int uniqueID = 0;

        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery("SELECT Auto_increment FROM information_schema.tables " +
                    "WHERE table_name = \'" + table + "\'");

            if (result.next()) {
                uniqueID = result.getInt(1);
            }
        } catch (SQLException ignored) {

        } finally {
            DatabaseConnectionHelper.closeConnection(connection);
        }

        return uniqueID;
    }

    /**
     * Method runs a <code>SELECT</code> query that selects all data in the
     * {@link DatabaseTable#MAPS_TABLE} and returns it wrapped in a
     * {@link MapData}.
     *
     * @return list of {@link MapData}
     */
    public static List<MapData> selectMapDataFromDatabase() {
        Connection connection = DatabaseConnectionHelper.getConnection();
        List<MapData> listOfMapData = new ArrayList<>();
        ResultSet result;

        try (Statement statement = connection.createStatement()) {
            result = statement.executeQuery(getSelectAllColumnsQuery(DatabaseTable.MAPS_TABLE));

            while (result.next()) {
                listOfMapData.add(getMapDataFromResultSet(result));
            }
        } catch (Exception ignored) {

        } finally {
            DatabaseConnectionHelper.closeConnection(connection);
        }

        return listOfMapData;
    }

    private static String getDeleteMapDataQuery(List<MapData> mapData) {
        return "DELETE FROM " + DatabaseTable.MAPS_TABLE + " WHERE "
                + getDeleteMapDataQueryWhereCondition(mapData);
    }

    private static String getDeleteMapDataQueryWhereCondition(List<MapData> mapData) {
        StringBuilder whereCondition = new StringBuilder();

        for (int i = 0, size = mapData.size(); i < size; i++) {
            whereCondition.append(DatabaseTable.MapsTable.ID).append(" = ").append(mapData.get(i).getMapID());

            if (i != size - 1) {
                whereCondition.append(" OR ");
            }
        }

        return whereCondition.toString();
    }

    private static String getInsertMapDataQuery(MapData mapData) {
        return "INSERT INTO " + DatabaseTable.MAPS_TABLE + " VALUES (" + mapData.getMapID() + ", " +
                mapData.getMapWidth() + ", " + mapData.getMapLength() + ", \"" + mapData.getMapData() +
                "\", \"" + mapData.getMapName() + "\")";
    }

    private static void addMapDataInsertionBatches(List<MapData> mapData, Statement statement) throws SQLException {
        for (MapData data : mapData) {
            statement.addBatch(getInsertMapDataQuery(data));
        }
    }

    private static String getSelectAllColumnsQuery(DatabaseTable table) {
        return "SELECT * FROM " + table + " WHERE 1";
    }

    private static MapData getMapDataFromResultSet(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(DatabaseTable.MapsTable.ID.toString());
        int width = resultSet.getInt(DatabaseTable.MapsTable.WIDTH.toString());
        int length = resultSet.getInt(DatabaseTable.MapsTable.LENGTH.toString());
        String data = resultSet.getString(DatabaseTable.MapsTable.DATA.toString());
        String name = resultSet.getString(DatabaseTable.MapsTable.NAME.toString());

        return new MapData(id, width, length, data, name);
    }
}
