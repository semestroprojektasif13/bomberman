package database.utility;

/**
 * This class wraps map data from simple data to a complete
 * <code>object</code> that contains useful methods for map's
 * data retrieval.
 *
 * @author Sarunas Sarakojis
 */
public class MapData {

    private final int mapID;
    private final int mapWidth;
    private final int mapLength;
    private final String mapData;
    private final String mapName;

    /**
     * Creates an instance of <code>this</code> class with the specified arguments.
     * <strong>Note</strong>, that if this map might be inserted into the database, then in order
     * to supply an unique ID {@link QueryExecutor#getUniqueIdentification(DatabaseTable)} should
     * be used.
     *
     * @param mapID a uniqueID
     * @param mapWidth width of a map
     * @param mapLength length of a map
     * @param mapData a <code>String</code> that contains all the block data
     * @param mapName name of a map
     */
    public MapData(int mapID, int mapWidth, int mapLength, String mapData, String mapName) {
        this.mapID = mapID;
        this.mapWidth = mapWidth;
        this.mapLength = mapLength;
        this.mapData = mapData;
        this.mapName = mapName;
    }

    public final int getMapID() {
        return mapID;
    }

    public final int getMapWidth() {
        return mapWidth;
    }

    public final int getMapLength() {
        return mapLength;
    }

    public final String getMapData() {
        return mapData;
    }

    public final String getMapName() {
        return mapName;
    }

    @Override
    public String toString() {
        return "ID: " + mapID + ", name: " + mapName + ", width: " + mapWidth + ", length: " + mapLength;
    }

    /**
     * Method checks if two instances of {@link MapData} are equal. Two maps
     * considered the same if they have the same <code>mapID</code>.
     *
     * @param obj another object to compare
     * @return true if the objects are equal, false if not
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof MapData && this.mapID == ((MapData) obj).mapID;
    }
}
