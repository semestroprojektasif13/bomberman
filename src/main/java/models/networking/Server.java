package models.networking;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Server functionality
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class Server extends Connection implements Runnable{
    private static boolean running = true;
    private static ServerSocket serverSocket;

    /**
     * setup server port for connection
     */
    public Server() {
        try {
            serverSocket = new ServerSocket(6789,100);
            waitForConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * terminate connection
     */
    public void terminate(){
        running = false;
        closeConnection();
    }

    /**
     * wait for client to connect to server port
     * @throws IOException
     */
    private void waitForConnection() throws IOException {
        setConnection(serverSocket.accept());
    }

    @Override
    public void run() {
        try {
            setupStreams();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (running) {
            try {
                whileConnected();
            } catch (Exception e) {
                terminate();
            }
        }
    }
}
