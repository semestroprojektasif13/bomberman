package models.networking;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
/**
 * Class for setting up network connectivity as a client
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class Client extends Connection implements Runnable {

    private static String serverIP;
    private static int port;
    private static boolean running = true;


    /**
     * establish connection with server
     *
     * @param serverIP-server IP address
     * @param port            - server port number
     */
    public Client(String serverIP, int port) {
        this.port = port;
        this.serverIP = serverIP;
        try {
            connectToServer();
            setupStreams();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * connect to server
     *
     * @throws IOException
     */
    private void connectToServer() throws IOException {
        setConnection(new Socket(InetAddress.getByName(serverIP), port));
    }

    /**
     * terminate connection
     */
    public void terminate() {
        running = false;
        closeConnection();
    }

    @Override
    public void run() {
        try {
            setupStreams();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (running) {
            try {
                whileConnected();
            } catch (Exception e) {
                terminate();
            }
        }
    }
}
