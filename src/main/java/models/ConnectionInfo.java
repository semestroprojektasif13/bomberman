package models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Class to store connection data
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class ConnectionInfo {
    private StringProperty ip;
    private StringProperty map;
    private StringProperty size;
    private StringProperty name;
    private IntegerProperty port;

    public final String getIP() {
        return ipProperty().get();
    }

    public final StringProperty ipProperty() {
        if (ip == null) {
            ip = new SimpleStringProperty();
        }
        return ip;
    }

    public final void setIP(String ip) {
        ipProperty().set(ip);
    }

    public final StringProperty mapProperty() {
        if (map == null) {
            map = new SimpleStringProperty();
        }
        return map;
    }

    public final String getMap() {
        return mapProperty().get();
    }

    public final void setMap(String map) {
        mapProperty().set(map);
    }

    public final StringProperty sizeProperty() {
        if (size == null) {
            size = new SimpleStringProperty();
        }
        return size;
    }

    public final String getSize() {
        return sizeProperty().get();
    }

    public final void setSize(String size) {
        sizeProperty().set(size);
    }

    public final StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty();
        }
        return name;
    }

    public final String getName() {
        return nameProperty().get();
    }

    public final void setName(String name) {
        nameProperty().set(name);
    }

    public final IntegerProperty portProperty() {
        if (port == null) {
            port = new SimpleIntegerProperty();
        }
        return port;
    }

    public final Integer getPort() {
        return portProperty().get();
    }

    public final void setPort(Integer port) {
        portProperty().set(port);
    }

    /**
     * setup object with data used for multiplayer connection setup
     *
     * @param ip   server ip address
     * @param map  map chosen for game
     * @param size map size
     * @param name server name
     * @param port port used to connect
     */
    public ConnectionInfo(String ip, String map, String size, String name, int port) {
        setIP(ip);
        setMap(map);
        setSize(size);
        setName(name);
        setPort(port);
    }
}
