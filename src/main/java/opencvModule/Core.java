package opencvModule;

import database.utility.DatabaseTable;
import database.utility.MapData;
import database.utility.QueryExecutor;
import opencvModule.core.MainLoop;
import opencvModule.imageOut.ImagePanelInterface;
import opencvModule.imageOut.ImageUtilities;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * Created by Saulius Stankevičius
 * Main OpenCV class
 */
public class Core
{
    private ImagePanelInterface imagePanel;
    private static MainLoop mainLoop;

    private static ExecutorService threadPool;
    private Future mapString;

    private boolean isAlive = false;

    private ArrayList<BufferedImage> imagesToOverlay = new ArrayList<>();

    /**
     * Constructor for the Core class
     * @param imagePanel the panel to display results on
     * @param imagesPathForOverlay paths to images to overlay on the analyzed image
     */
    public Core(ImagePanelInterface imagePanel, ArrayList<String> imagesPathForOverlay)
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        this.imagePanel = imagePanel;
        this.imagesToOverlay = ImageUtilities.loadImages(imagesPathForOverlay);
    }

    /**
     * Opens video feed and analyzes the captured image, runs infinite loop
     */
    public void analyze()
    {
        this.isAlive = true;
        threadPool = Executors.newFixedThreadPool(3);
        mainLoop = new MainLoop(imagePanel, imagesToOverlay);
        mainLoop.setRun(true);
        mapString = threadPool.submit(mainLoop);
    }

    /**
     * Analyzes the given image
     * @param imagePath path to image to analyze
     */
    public void analyze(String imagePath)
    {
        this.isAlive = true;
        threadPool = Executors.newFixedThreadPool(3);
        mainLoop = new MainLoop(ImageUtilities.loadImage(imagePath), imagePanel, imagesToOverlay);
        mainLoop.setRun(true);
        mapString = threadPool.submit(mainLoop);
    }

    /**
     * Returns the latest result and stops the loop if it is running
     * @return map in string
     */
    public MapData getMapData()
    {
        MapData mapData = null;

        if(this.isAlive)
        {
            this.isAlive = false;
            mainLoop.setRun(false);

            try
            {
                String output = (String) mapString.get();

                if(output != null)
                {
                    String[] data = output.split(" ");
                    mapData = new MapData(QueryExecutor.getUniqueIdentification(DatabaseTable.MAPS_TABLE),
                            Integer.parseInt(data[0]), Integer.parseInt(data[1]), data[2], "");
                }

                threadPool.shutdown();
            }
            catch (InterruptedException | ExecutionException e)
            {
                e.printStackTrace();
            }
        }

        return mapData;
    }

    /**
     * Stops the loop
     */
    public void stop()
    {
        if(isAlive)
        {
            mainLoop.setRun(false);
            this.isAlive = false;
            threadPool.shutdown();
        }
    }
}
