package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Enemy extends GameObject {

    private int helper[][] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    private char mapEnemy[][] = new char[18][18];

    private boolean isMovingRandomly = false;

    private Random random;

    private Handler handler;

    private char number[] = {'P', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', '+', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'};

    public Enemy(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 30, 30);
    }

    public Rectangle getBounds(int x, int y) {
        return new Rectangle(x, y, 30, 30);
    }

    public void tick() {
        if (!getDead() && !getWonTheGame() && !getRunOutOfTime()) {
            int n;

            if (x % 30 == 0 && y % 30 == 0/* && isEnamySurroundedByBloks()*/) {
                reDraw();

                n = 0;
                while (calculateWayToPlayer(n)) {
                    n++;
                }

                if (n == number.length - 2) {
                    isMovingRandomly = true;
                } else {
                    isMovingRandomly = false;
                }
                if (!isMovingRandomly) {
                    moveTowardsPlayer(number[n]);
                }

            }

            if (isMovingRandomly || (velx == 0 && vely == 0)) {
                moveRandomly();
            }
            collision();

            x += velx;
            y += vely;

        }
    }

    public void render(Graphics g) {
        direction();
        if (getDead()) {
            drawEnamyImage(g, getDeadAnimation(handler), 4);
        } else {
            if (getWonTheGame() || getRunOutOfTime()) {
                drawEnamyImage(g, 0, getDirection());
            } else {
                drawEnamyImage(g, getAnimation(150), getDirection());
            }
        }
    }

    public void direction() {
        if (velx > 0) {
            setDirection(0);
        } else if (velx < 0) {
            setDirection(2);
        }
        if (vely > 0) {
            setDirection(1);
        } else if (vely < 0) {
            setDirection(3);
        }
    }

    private boolean moveRandomly() {
        random = new Random();
        int xx = velx * -1, yy = vely * -1;

        while (xx == velx * -1 && yy == vely * -1) {
            switch (random.nextInt(4)) {
                case 0:
                    xx = 1;
                    yy = 0;
                    break;
                case 1:
                    xx = 0;
                    yy = 1;
                    break;
                case 2:
                    xx = -1;
                    yy = 0;
                    break;
                case 3:
                    xx = 0;
                    yy = -1;
                    break;
            }
        }

        boolean isColliding = false;
        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (getBounds(x + xx, y + yy).intersects(tempObject.getBounds()) && tempObject != Enemy.this) {
                isColliding = true;
            }

        }

        if (!isColliding) {
            velx = xx;
            vely = yy;
            return true;
        }
        return false;
    }

    public boolean calculateWayToPlayer(int n) {

        for (int i = 0; i < mapEnemy.length; i++) {
            for (int ii = 0; ii < mapEnemy[0].length; ii++) {
                if (mapEnemy[i][ii] == number[n]) {
                    if (lookForChar(' ', number[n + 1], i, ii) == false) {
                        return false;
                    }
                }
            }
        }

        if (number.length == n + 2) {
            return false;
        } else {
            return true;
        }
    }

    private boolean lookForChar(char ch, char ch2, int xx, int yy) {
        for (int i = 0; i < helper.length; i++) {
            if (mapEnemy[xx + helper[i][0]][yy + helper[i][1]] == ch) {
                mapEnemy[xx + helper[i][0]][yy + helper[i][1]] = ch2;
            }
            if (mapEnemy[xx + helper[i][0]][yy + helper[i][1]] == 'Z') {
                return false;
            }
        }
        return true;
    }

    public void moveTowardsPlayer(char ch) {

        for (int i = 0; i < helper.length; i++) {
            if (mapEnemy[x / 30 + helper[i][0]][y / 30 + helper[i][1]] == ch) {
                velx = helper[i][0];
                vely = helper[i][1];
            }
        }
    }

    public void reDraw() {
        for (int j = 0; j < mapEnemy.length; j++) {
            for (int jj = 0; jj < mapEnemy[0].length; jj++) {
                mapEnemy[j][jj] = ' ';
            }
        }

        for (int j = 0; j < mapEnemy.length; j++) {
            for (int jj = 0; jj < mapEnemy[0].length; jj++) {
                for (int i = 0; i < handler.getObject().size(); i++) {
                    GameObject tempObject = handler.getObject().get(i);
                    if (getBounds(j * 30, jj * 30).intersects(tempObject.getBounds())) {
                        if (tempObject.getId() == ID.Player2 || tempObject.getId() == ID.Player) {
                            mapEnemy[j][jj] = 'P';
                        } else if (tempObject.getId() == ID.Enemy && tempObject == Enemy.this) {
                            mapEnemy[j][jj] = 'Z';
                        } else {
                            mapEnemy[j][jj] = '#';
                        }
                    }
                }
            }
        }

    }

    private void collision() {
        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (tempObject.getId() == ID.BombExplode) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    // handler.remuveObject(this);
                    setDead(true);
                    nullImagetime();
                }
            }

            // jei du priesai susiduria
            if (tempObject.getId() == ID.Enemy && tempObject != Enemy.this) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    x -= velx * 2;
                    y -= vely * 2;
                    velx *= -1;
                    vely *= -1;
                }
            }
            if (!(tempObject.getId() == ID.Player || tempObject.getId() == ID.Player2)) {
                if (getBounds().intersects(tempObject.getBounds()) && tempObject != Enemy.this) {
                    x -= velx;
                    y -= vely;
                    velx = 0;
                    vely = 0;
                }
            }
            if (tempObject.getId() == ID.Player || tempObject.getId() == ID.Player2) {
                if (getBounds().intersects(tempObject.getBounds()) && tempObject != Enemy.this) {
                    x -= velx;
                    y -= vely;
                }
            }
        }
    }
}