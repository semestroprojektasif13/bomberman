package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Block extends GameObject {

    private Handler handler;

    private Random random;

    private boolean delete = false;

    public Block(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 30, 30);
    }

    public void tick() {
        boolean del = collision();
        if (del && delete) {
            generateBomb(x, y);
            handler.remuveObject(this);
        } else if (!del) {
            delete = true;
        }

    }

    public void render(Graphics g) {
        drawBlockImage(g, 0);
    }

    private void generateBomb(int xx, int yy) {
        random = new Random();

        if (Game.giftFortune > random.nextInt(100)) {
            switch (random.nextInt(5)) {
                case 0:
                    handler.addObject(new GiftBomb(xx, yy, ID.GiftBomb, handler, 50));
                    break;
                case 1:
                    handler.addObject(new GiftKick(xx, yy, ID.GiftKick, handler, 50));
                    break;
                case 2:
                    handler.addObject(new GiftSpeed(xx, yy, ID.GiftSpeed, handler, 50));
                    break;
                case 3:
                    handler.addObject(new GiftBombDemage(xx, yy, ID.GiftBombDemage, handler, 50));
                    break;
                case 4:
                    handler.addObject(new GiftThrowBomb(xx, yy, ID.GiftThrowBomb, handler, 50));
                    break;
            }
        }

    }

    private boolean collision() {

        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (tempObject.getId() == ID.BombExplode) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    return false;
                }
            }

        }
        return true;
    }
}