package bomberman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Border extends GameObject {

    private int width, height;

    public Border(int x, int y, ID id, int width, int height) {
        super(x, y, id);
        this.height = height;
        this.width = width;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    public void tick() {

    }

    public void render(Graphics g) {
        g.setColor(Color.blue);
        g.fillRect(x, y, width, height);

        //  for(int i = 0; i < height; i+=30){
        //  drawBlockImage(g, 1);
        // }
    }
}

