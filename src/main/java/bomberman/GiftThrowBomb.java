package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;

public class GiftThrowBomb extends GameObject {

    private Handler handler;

    public GiftThrowBomb(int x, int y, ID id, Handler handler, int time) {
        super(x, y, id);
        this.handler = handler;
        isItBomb = true;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 30, 30);
    }

    public void tick() {

        collision();
    }

    public void render(Graphics g) {
        drawGiftImage(g, getAnimation(150), 3);
    }

    private void collision() {
        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (tempObject != GiftThrowBomb.this) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    handler.remuveObject(this);
                }
            }

        }
    }
}

/*
        for(int i = 0; i < Game.map.length; i++){
            for(int ii = 0; ii < Game.map[0].length; ii++)
                 System.out.print(Game.map[i][ii]+" ");
                 System.out.print("\n");
            }
        System.out.print("\n\n"); 
*/
