package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;

public class BombExplode extends GameObject {

    private int direction;

    private int imageTime = 0;

    private Handler handler;

    public BombExplode(int x, int y, ID id, Handler handler, int direction) {
        super(x, y, id);
        this.handler = handler;
        this.direction = direction;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 30, 30);
    }

    public void tick() {
        addTime();
        if (getTime() % 10 == 0) {
            imageTime++;
        }
        if (getTime() == 50) {
            handler.remuveObject(this);
        }
    }

    public void render(Graphics g) {
        drawBombExImage(g, imageTime, direction);
    }
}