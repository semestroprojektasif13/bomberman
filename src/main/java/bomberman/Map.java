package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Map extends GameObject {

    public Map(int x, int y, ID id) {
        super(x, y, id);
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 30, 30);
    }

    public void tick() {
    }

    public void render(Graphics g) {
        drawBlockImage(g, 1);
    }

}