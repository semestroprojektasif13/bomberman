package bomberman;

// TODO Java emums are in UPPER_CASE
public enum ID {
        // BOMB
            Bomb(),
        // PLAYER
	Player(),
        Player2(),
	Enemy(),
        Map(),
        Block(),
        Border(),
        BombExplode(),
        
        GiftBomb(),
        GiftSpeed(),
        GiftKick(),
        GiftBombDemage(),
        GiftThrowBomb()
}
