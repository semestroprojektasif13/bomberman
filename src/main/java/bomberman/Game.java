package bomberman;

import database.utility.MapData;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;
import java.util.Random;

public class Game extends Canvas implements Runnable {

    public static final int chunk = 30;

    public static int w = 2 * 30; //50%

    public static final int WIDTH = 466, HEIGHT = 489 + w; // perfect WIDTH = 466; HEIHGT = 489;

    private static final long serialVersionUID = 5402025502957152942L;

    public static int giftFortune = 50; //50%

    public static int scale = 13; //50%

    public static boolean running = false;

    private Random random;

    // Menu menu;
    private Handler handler;

    private Thread thread;

    public Game() {
        // menu = new Menu();
        handler = new Handler();
        this.addKeyListener(new KeyInput(handler));
        new Window(WIDTH, HEIGHT, "☻", this);

        handler.addObject(new Player(30, 30 + w, ID.Player, handler));
        handler.addObject(new Player(390, 390 + w, ID.Player2, handler));

        handler.addObject(new Border(-60, 30 + w, ID.Border, 90, 13 * 30));
        handler.addObject(new Border(30, -60 + w, ID.Border, 13 * 30, 90));
        handler.addObject(new Border(30, 14 * 30 + w, ID.Border, 13 * 30, 90));
        handler.addObject(new Border(14 * 30, 30 + w, ID.Border, 90, 13 * 30));

        for (int i = 60; i < 60 + 60 * 6; i += 60) {
            for (int ii = 60; ii < 60 + 60 * 6; ii += 60) {
                handler.addObject(new Map(i, ii + w, ID.Map));
            }
        }

        createBlocks(80, 4, w / 30);
        handler.addObject(new Header(ID.Map, handler));

    }

    public Game(MapData mapData) {
/*
        int width = mapData.getMapWidth() + 1;
        int height = mapData.getMapLength() + 1;
        int index = 0;
        handler = new Handler();

        this.addKeyListener(new KeyInput(handler));
        new Window(widthchunk + chunk2 - 15, heightchunk + chunk2, BomberMan, this);

        for (int i = chunk; i < widthchunk; i += chunk) {
            for (int ii = chunk; ii < heightchunk; ii += chunk) {
                if (index < mapData.getMapData().length()) {
                    switch (mapData.getMapData().charAt(index)) {
                        case '1':
                            handler.addObject(new Map(ii, i, ID.Map));
                            break;
                        case '2':
                            handler.addObject(new Block(ii, i, ID.Block, handler));
                            break;
                        case '3':
                            handler.addObject(new Enemy(ii, i, ID.Enemy, handler));
                            break;
                        case '4':
                            handler.addObject(new Player(ii, i, ID.Player, handler));
                            break;
                    }
                    System.out.println(index);
                    index++;
                }

            }
        }

        handler.addObject(new Border(-chunk * 2, 0, ID.Border, chunk * 3, height * chunk));
        handler.addObject(new Border(0, -chunk * 2, ID.Border, width * chunk, chunk * 3));
        handler.addObject(new Border(0, width * chunk, ID.Border, width chunk + chunk * 2, chunk * 3));
        handler.addObject(new Border(height * chunk, 0, ID.Border, chunk * 3, height * chunk));
*/
    }

    public static void main(String args[]) {
        new Game();
    }

    public synchronized void start() {
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    public synchronized void stop() {
        try {
            thread.join();
            running = false;
        } catch (Exception e) {
            // TODO Logger
            e.printStackTrace();
        }
    }

    public void run() {
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 3) {
                tick();
                delta--;
            }
            if (running) {
                render();
            }

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
            }

        }

        stop();
    }

    private void tick() {
        handler.tick();
    }

    private void render() {

        BufferStrategy bs = this.getBufferStrategy();
        if (bs == null) {
            this.createBufferStrategy(3);
            return;
        }

        Graphics g = bs.getDrawGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, HEIGHT);

        // Image img = new ImageIcon("images/background3.png").getImage();
        //  for(int i = 0; i < 13; i++)
        //     for(int ii = 0; ii < 13; ii++)
        //    g.drawImage(img, 30+i*30, 30+w+ii*30, this);
        g.setColor(Color.blue);

        handler.render(g);

        g.dispose();
        bs.show();
    }

    private void createBlocks(int blocks, int enamies, int h) {
        int emptyBlocks[][] = {{1, 1}, {1, 2}, {2, 1}, {13, 13}, {13, 12}, {12, 13}};
        int xx, yy, i;
        random = new Random();
        boolean f;

        for (int j = 0; j < blocks; j++) {
            f = true;
            xx = random.nextInt(13) + 1;
            yy = random.nextInt(13) + 1 + h;
            for (i = 0; i < emptyBlocks.length; i++) {
                if (emptyBlocks[i][0] == xx && emptyBlocks[i][1] + h == yy) {
                    f = false;
                }
            }

            for (i = 0; i < handler.getObject().size(); i++) {
                if (handler.getObject().get(i).getBounds().intersects(new Rectangle(xx * 30, yy * 30, 30, 30))) {
                    f = false;
                }
            }
            if (f) {
                if (j < enamies) {
                    handler.addObject(new Enemy(xx * 30, yy * 30, ID.Enemy, handler));
                } else {
                    handler.addObject(new Block(xx * 30, yy * 30, ID.Block, handler));
                }
            } else {
                j--;
            }
        }

    }
}
