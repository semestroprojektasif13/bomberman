package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;

public class GiftSpeed extends GameObject {

    private Handler handler;

    public GiftSpeed(int x, int y, ID id, Handler handler, int time) {
        super(x, y, id);
        this.handler = handler;
        isItBomb = true;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 30, 30);
    }

    public void tick() {
        collision();
    }

    public void render(Graphics g) {
        drawGiftImage(g, getAnimation(150), 1);
    }

    private void collision() {
        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (tempObject != GiftSpeed.this) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    handler.remuveObject(this);
                }
            }

        }
    }
}
